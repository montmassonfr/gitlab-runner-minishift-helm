# Pré-requis

Installer [Minishift](https://docs.okd.io/latest/minishift/getting-started/index.html)

# Pour démarrer

cloner ce projet

```powershell
PS D:\projects\> git clone https://gitlab.com/montmassonfr/gitlab-runner-minishift-helm/
PS D:\projects\> cd gitlab-runner-minishift-helm 
```

# Espace de travail dédié

Création d'un projet avec l'utilisateur developper de minishift
1. Connectez-vous à l'interface graphique fourni lors de l'execution de la commande minishift start avec l'utilisateur developer
2. Cliquer sur ajouter un projet et nommer le gitlab-runner-minishift-helm-build

# Authoriser anyuid pour service account exécution gitlab-runner

Dans une console connecté vous en admin et authoriser le service account build dans le namespace 
gitlab-runner-minishift-helm-build à executer des images avec n'importe quel uid.

```powershell
eval $(minishift oc-env)
oc login -u system:admin
oc adm policy add-scc-to-user anyuid -z build -n gitlab-runner-minishift-helm-build
```

# Connecter vous avec oc developper

Copy Login command est utilisé la dans votre console favorite

```powershell
eval $(minishift oc-env)
oc login https://192.168.99.101:8443 --token=TslVaiZV9NSRYK5gDjqgeMjjlTVbl93RQf-vjV-0BNo
oc project gitlab-runner-minishift-helm-build
```

Création:
* d'un namespace  gitlab-runner-minishift-helm-build
* d'un service account build
* et attribution du role admin dans ce namespace

```powershell
kubectl apply -f rbac-config.yaml --record
```

# Configurer helm

## Initialise Tiller 

Cette commande a ajoute le pod tiller avec le service account build dans le namespace gitlab-runner-minishift-helm-build

```powershell
helm init --service-account build --tiller-namespace gitlab-runner-minishift-helm-build 
```

## Ajout du repo helm gitlab

```powershell
helm --tiller-namespace gitlab-runner-minishift-helm-build repo add gitlab https://charts.gitlab.io
```

# Installation du runner

Avant d'execution la commande ci-dessous veuillez saisir le token d'enregistrement.
Récuperer les information d'enregistrement sous [Settings -> CI/CD](https://gitlab.com/montmassonfr/gitlab-runner-minishift-helm/settings/ci_cd)  dans la section Runner. Specific Runners -> Setup a specific Runner manually
Afin de garantir l'utilisation de votre runner, cliquer sur le bouton disable shared runner.

```powershell
helm install --tiller-namespace gitlab-runner-minishift-helm-build --namespace gitlab-runner-minishift-helm-build --name gitlab-runner -f values.yaml gitlab/gitlab-runner
```

# Upgrade

```powershell
helm upgrade --namespace gitlab-runner-minishift-helm-build --tiller-namespace gitlab-runner-minishift-helm-build -f values.yaml gitlab-runner gitlab/gitlab-runner
```

# Nettoyage 

```powershell
kubectl delete ns gitlab-runner-minishift-helm-build
```
